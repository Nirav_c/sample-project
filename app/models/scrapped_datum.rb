class ScrappedDatum < ActiveRecord::Base

	validates_presence_of :url

	def self.get_all
    	ScrappedDatum.all.as_json(except: [:created_at, :updated_at, :id])
  	end

end
