class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception

    def render_json(response, message, status_code)
    	render json: {result: response, messages: message}, status: status_code
  	end
end
