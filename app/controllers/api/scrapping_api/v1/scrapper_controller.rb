module Api::ScrappingApi::V1
	class ScrapperController < ApplicationController
		
		def get_list
			render json: {
				scrapped_data_list: ScrappedDatum.get_all.each{ |a| a['data'] = JSON.parse(a['data']) },
          		messages: ['Success']
          	},status: :ok and return
		end

		def scrap
			require 'open-uri'
			require 'uri'

			if params['scrapper']['url'].blank?
				render_json("Failed",['URL not found'],:bad_request) and return
			end

			if URI.parse(params['scrapper']['url']).host.nil?
				render_json("Failed",['Invalid URL'],:bad_request) and return
			end	

			content = Nokogiri::HTML(open(params['scrapper']['url']))
			
			obj = Hash.new
			obj = {h1: [], h2: [], h3: [], a: []}
			obj.each do |key,val|
				content.css(key).each do |elem,index| 
					obj[key].push(elem.text.strip) unless elem.text.blank?
				end
			end

			@scrapped_data = ScrappedDatum.new
			@scrapped_data.url = params['scrapper']['url']
			@scrapped_data.data = obj.to_json

			if @scrapped_data.valid?
				@scrapped_data.save!
			else
				render_json("Failed",["The result does not save due to errors. Please try again."],:bad_request) and return	
			end	
			
			render_json("Success",['Data scrapped from given url'],:ok) and return
			
		end	
	end
end
