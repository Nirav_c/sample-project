What is this repository for?

This repository provides the basic understanding of scrapping data from website. It consists of RESTful API with 2 useful endpoints. Here is the detail for the same:

    scrap: This endpoint takes URL as an input parameter, extracts the text content from h1,h2,h3 and anchor tags and saves as the stringified JSON object in the database. The format of the input parameter should be following:
      {
         "scrapper": {
	           "url": "your URL"
          }
      }

    get_list: It returns the listing of all URLs and scrapped data in JSON format. The format will be: 
        {
            "scrapped_data_list": [
                {
                    "url": "http://getbootstrap.com/",
                    "data": {
                        "h1": [],
                        "h2": [
                            "Designed for everyone, everywhere."
                        ],
                        "h3": [
                            "Preprocessors",
                            "One framework, every device.",
                            "Full of features"
                        ],
                        "a": [
                            "Components",
                            "JavaScript",
                            "Customize",
                            "Themes",
                            "Expo",
                            "Blog",
                        ]
                    }
                }
            ],
            "messages": [
                "Success"
            ]
      }



How do I get set up?

Just clone the repository and start using API !!!

Dependencies:

The system should have RubyOnRails setup with postgresql.