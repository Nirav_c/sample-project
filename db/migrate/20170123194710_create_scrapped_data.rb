class CreateScrappedData < ActiveRecord::Migration
  def change
    create_table :scrapped_data do |t|
      t.string :url
      t.text :data

      t.timestamps null: false
    end
  end
end
